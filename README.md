# CSS Workshop - CSS Grid & 12 Column styling

Recording: [Teams Meeting](https://blubitoag-my.sharepoint.com/personal/p_ivanova_blubito_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fp%5Fivanova%5Fblubito%5Fcom%2FDocuments%2FAufnahmen%2FFront%2Dend%20guild%20%2D%20Regular%20Meeting%2D20211109%5F110101%2DMeeting%20Recording%2Emp4&parent=%2Fpersonal%2Fp%5Fivanova%5Fblubito%5Fcom%2FDocuments%2FAufnahmen)

In this project we're showing how to use so pretty widely known "Bootstrap 12 column grid" system, how to make a clone of it and how to completely forget about it via CSS Grid.

## Examples

```
src/components/GridBootstrap.vue - Bootstrap Grid
src/components/GridCloneBootstrap.vue -Clone of Bootstrap Grid with CSS Grid
src/components/GridCSS.vue - Simple CSS Grid example
```

## Start te project

```
1) install or yarn install
2) npm run dev OR yarn dev
```
